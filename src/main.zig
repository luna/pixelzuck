const std = @import("std");

const ClientMap = std.AutoHashMap(usize, *Client);
const Client = struct {
    allocator: *std.mem.Allocator,
    client_map: *ClientMap,
    id: usize,
    conn: std.net.StreamServer.Connection,
};

const RGB = struct {
    r: u8,
    g: u8,
    b: u8,

    const Self = @This();

    pub fn fromHex(hex_string: []const u8) !Self {
        return Self{
            .r = std.fmt.parseInt(u8, hex_string[0..2], 16) catch return error.InvalidColor,
            .g = std.fmt.parseInt(u8, hex_string[2..4], 16) catch return error.InvalidColor,
            .b = std.fmt.parseInt(u8, hex_string[4..6], 16) catch return error.InvalidColor,
        };
    }

    pub fn format(
        self: Self,
        comptime fmt: []const u8,
        _: std.fmt.FormatOptions,
        writer: anytype,
    ) !void {
        if (fmt.len == 0 or comptime std.mem.eql(u8, fmt, "s")) {
            try std.fmt.format(
                writer,
                "{x:002}{x:002}{x:002}",
                .{ self.r, self.g, self.b },
            );
        } else {
            @compileError("Unknown format character: '" ++ fmt ++ "'");
        }
    }
};

fn posToPosition(x: usize, y: usize) usize {
    return (y * 1280 + x) * 4;
}

fn getPixel(framebuffer_file: std.fs.File, x: usize, y: usize) !RGB {
    const pos = posToPosition(x, y);
    var rgb_buffer: [4]u8 = undefined;
    try framebuffer_file.seekTo(pos);
    const bytes_read = try framebuffer_file.reader().read(&rgb_buffer);
    std.debug.assert(bytes_read == 4);

    return RGB{
        .r = rgb_buffer[2],
        .g = rgb_buffer[1],
        .b = rgb_buffer[0],
    };
}

fn setPixel(framebuffer_file: std.fs.File, x: usize, y: usize, color: RGB) !void {
    const pos = posToPosition(x, y);
    const rgb_buffer = [4]u8{ color.b, color.g, color.r, 255 };

    try framebuffer_file.seekTo(pos);
    _ = try framebuffer_file.writer().write(&rgb_buffer);
}

const logger = std.log.scoped(.pixelzuck);

fn handleSingleClient(client: *Client) void {
    handleSingleClientLoop(client) catch |err| {
        logger.err("error handling client: {s}", .{@errorName(err)});
        if (@errorReturnTrace()) |trace| {
            std.debug.dumpStackTrace(trace.*);
        }

        // send the error to the client and ignore if they are crashed
        client.conn.stream.writer().print("error: {s}", .{@errorName(err)}) catch {};

        client.conn.stream.close();
        client.allocator.destroy(client);
    };
}

fn handleSingleClientLoop(client: *Client) !void {
    const framebuffer_file = try std.fs.cwd().openFile(
        "/dev/fb0",
        .{ .read = true, .write = true },
    );
    defer framebuffer_file.close();

    while (true) {
        var reader = client.conn.stream.reader();
        var writer = client.conn.stream.writer();

        // parse multiple lines given in the reader stream
        while (true) {
            var line_buffer: [32]u8 = undefined;
            const message_line_opt = reader.readUntilDelimiterOrEof(&line_buffer, '\n') catch return error.InvalidMessage;
            if (message_line_opt == null) break;
            const message_line_received = message_line_opt.?;

            const line = std.mem.trimRight(u8, message_line_received, "\r");
            var parts_it = std.mem.split(line, " ");
            logger.info("got message: {s}", .{line});

            const command = parts_it.next() orelse return error.InvalidMessage;

            if (std.mem.eql(u8, command, "HELP")) {
                _ = try writer.write("Luna Pixelflut written for DENIS\n");
            } else if (std.mem.eql(u8, command, "SIZE")) {
                _ = try writer.write("SIZE 1280 720\n");
            } else if (std.mem.eql(u8, command, "PX")) {
                const pixel_x = std.fmt.parseInt(usize, parts_it.next() orelse return error.InvalidMessage, 10) catch return error.InvalidMessage;
                const pixel_y = std.fmt.parseInt(usize, parts_it.next() orelse return error.InvalidMessage, 10) catch return error.InvalidMessage;
                const color_str = parts_it.next();
                if (color_str == null) {
                    const rgb = try getPixel(framebuffer_file, pixel_x, pixel_y);
                    try writer.print("PX {d} {d} {s}\n", .{ pixel_x, pixel_y, rgb });
                } else {
                    const rgb = RGB.fromHex(color_str.?) catch return error.InvalidMessage;
                    try setPixel(framebuffer_file, pixel_x, pixel_y, rgb);
                }
            }
        }
    }
}

pub fn main() anyerror!void {
    var allocator_instance = std.heap.GeneralPurposeAllocator(.{}){};
    defer {
        _ = allocator_instance.deinit();
    }
    const allocator = &allocator_instance.allocator;

    var server = std.net.StreamServer.init(.{ .reuse_address = true });
    defer server.deinit();

    var addr = try std.net.Address.parseIp("0.0.0.0", 6900);
    try server.listen(addr);

    var client_map = ClientMap.init(allocator);
    defer client_map.deinit();

    const seed = @truncate(u64, @bitCast(u128, std.time.nanoTimestamp()));
    var r = std.rand.DefaultPrng.init(seed);

    while (true) {
        var conn = try server.accept();
        var client_id = r.random.uintLessThan(usize, 10000000);
        var owned_client = try allocator.create(Client);
        owned_client.* = .{
            .allocator = allocator,
            .client_map = &client_map,
            .id = client_id,
            .conn = conn,
        };

        // TODO: limit the amount of threads LOL

        try client_map.put(client_id, owned_client);
        _ = try std.Thread.spawn(.{}, handleSingleClient, .{owned_client});
    }
}
